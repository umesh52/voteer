﻿CREATE TABLE [dbo].[db_candidate]
(
	[Id] INT NOT NULL PRIMARY KEY IDENTITY, 
    [first_name] NVARCHAR(50) NULL, 
    [last_name] NVARCHAR(50) NULL, 
    [gender] NCHAR(10) NULL, 
    [consistuency] NVARCHAR(50) NULL, 
    [age] NCHAR(10) NULL, 
    [election_type] NVARCHAR(50) NULL, 
    [income] NCHAR(10) NULL, 
    [verifified] NVARCHAR(50) NOT NULL DEFAULT 'NO', 
    [cases] NVARCHAR(100) NULL
)
